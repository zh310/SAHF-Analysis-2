Fiji plugin to perform SAHF (Senescence-associated heterochromatin foci) phenotype classification.

It was based on the fluorescent signal manifest from cell nuclei, and made use of the statistical 
features to distinguish different patterns of fluorescence.


![SAHF sample image][img1]

[img1]: https://gitlab.developers.cam.ac.uk/zh310/SAHF-Analysis/-/raw/master/img/SAHF-rawdata.png "SAHF sample data"