/* 	Merge multi-channel images from ZEN export
 *  version 1.1.0
 *  2018.05.25
 */

dir = getDirectory("Choose the input directory");
//path = "C:/Work/Aled_Parry/test_z1-3/image_sequences/";
//newDir = path + "Measurement_Result" + File.separator;
//output = "C:/Work/Aled_Parry/Measurement_Result/";
//output = "C:/Work/Aled_Parry/image_sequences/";
newDir = dir + "channelMerged" + File.separator;
//newDir = dir + "channelMerged";

start = getTime();

if (!File.exists(newDir)) 
	File.makeDirectory(newDir);


//if (!File.exists(newDir)) 
//File.makeDirectory(newDir);

list = getFileList(dir);
//print(list.length/3);
setBatchMode(true);
count = 0;
mIdx = newArray(list.length);
preFix = "";
for (i=0; i<list.length; i++) {

	currentFileName = list[i];
	if (endsWith(currentFileName,"tif")) {
		
		
		//headIdx = indexOf(currentFileName, "c",30);
		//tailIdx = indexOf(currentFileName, "_ORG.tif");
		
		preFix = substring(currentFileName,0,lastIndexOf(currentFileName,"c"));

		FileName = substring(currentFileName,lastIndexOf(currentFileName,File.separator)+1,lengthOf(currentFileName));

		if (lastIndexOf(FileName,"m")!=-1) {
			mIdx[count] = substring(FileName,lastIndexOf(FileName,"m")+1,lastIndexOf(FileName,"_ORG"));
			count++;
		}
		else {
			mIdx[0] = 1;
		}
		
	}
}

if (count!=0) {
	mIdx = Array.slice(mIdx,0,count);
	//print(mIdx.length);

	unique_mIdx = getUniqueArrayValues(mIdx, false);
}
else {
	preFix = substring(preFix,0,lengthOf(preFix)-1);
	newFileName = newDir + preFix;
	print(newFileName);
	c1 = preFix + "_c1_ORG.tif";
	c2 = preFix + "_c2_ORG.tif";
	c3 = preFix + "_c3_ORG.tif";
	c4 = preFix + "_c4_ORG.tif";
	open(dir + c1);
	open(dir + c2);
	open(dir + c3);
	open(dir + c4);
	run("Merge Channels...", "c1=["+c1+ "] c2=["+c2+ "] c3=["+c3+ "] c4=["+c4+ "] create");
    saveAs("tiff", newFileName);
    close(); 
    setBatchMode(false);
    run("Collect Garbage");
    print("Script ends at: " + (getTime()-start)/1000 + " seconds.");
    return;
}
//print(preFix);
//for (i=0; i<unique_mIdx.length; i++) {
//	print(preFix + "m" + unique_mIdx[i]);
//}

for (i=0; i<unique_mIdx.length; i++) {
	if (unique_mIdx[i]!=0){
		//print(preFix + "m" + mIdx[i]);
		newFileName = newDir + preFix + "m" + unique_mIdx[i];
		if (i==0)
			print(newFileName);
		else
			print("\\Update:" + newFileName);
		//print(unique_mIdx[i]);
		//preFix = substring(currentFileName, 0, headIdx-1);
		//open(path+filename[i]);
		c1 = preFix + "c1m" + unique_mIdx[i] + "_ORG.tif";
		c2 = preFix + "c2m" + unique_mIdx[i] + "_ORG.tif";
		c3 = preFix + "c3m" + unique_mIdx[i] + "_ORG.tif";
		c4 = preFix + "c4m" + unique_mIdx[i] + "_ORG.tif";
		open(dir + c1);
		open(dir + c2);
		open(dir + c3);
		open(dir + c4);
		
		run("Merge Channels...", "c1=["+c1+ "] c2=["+c2+ "] c3=["+c3+ "] c4=["+c4+ "] create");
    	//run("Save", "save=C:/Work/Yoko_Ito/Composite.tif");

		//newFileName = newDir+preFix + "m" + i;

    	saveAs("tiff", newFileName);
    	close();  
    	run("Collect Garbage");
	}
}

setBatchMode(false);
run("Collect Garbage");
// timing the end
print("Script ends at: " + (getTime()-start)/1000 + " seconds.");

//function returnes the unique values of an array in alphabetical order
//example: myUniqueValues = getUniqueArrayValues(myList, true);
function getUniqueArrayValues(inputArray, displayList) {
outputArray = newArray(inputArray[0]);		//outputArray stores unique values, here initioalitaion with first element of inputArray 
for (i = 1; i < inputArray.length; i++) {
	j = 0;									//counter for outputArray
	valueUnique = true;						//as long as value was not found in array of unique values
	while (valueUnique && (outputArray.length > j)) {   //as long as value was not found in array of unique values and end of array is not reached
		if(outputArray[j] == inputArray[i]) {
			valueUnique = false;			//if value was found in array of unique values stop while loop
			} else {
			j++;
			}
		}  //end while
	if (valueUnique) outputArray = Array.concat(outputArray,inputArray[i]);  //if value was not found in array of unique values add it to the end of the array of unique values
	}
print(outputArray.length + " unique values found."); 
Array.sort(outputArray);
if (displayList) {Array.show("List of " + outputArray.length + " unique values", outputArray);}	
return outputArray;
}

