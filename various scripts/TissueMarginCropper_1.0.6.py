###	python FIJI plugin to crop out the margin region of tissue images
#
#	It works preferably on multi-channel image stacks. The plugin takes one reference
#	channel or RGB composite image to check corners and border of the tissue region.
#	If the mean signal intensity of a certain corner (a triangle with user-define side 
#	length) comparing to the entire image is lower than a certain threshold, then this 
#	corner will be recognized as tissue-void margin.
#	The plugin will evaluate the 4 corners of the current image, guess the tissue layout,
#	and decide the best way to crop out the margin. The size of cropping from the tissue
#	margin is 100 micron, if not specified otherwise by the user.
#
#	version 1.0.5
#	Author: Ziqiang Huang <Ziqiang.Huang@cruk.cam.ac.uk>
#	Date: 2018.06.14
#
###############################################################

import os
from ij import IJ
from ij import ImagePlus

from ij.gui import Roi, PolygonRoi, Toolbar
from ij.io import FileSaver
from ij.plugin import RoiEnlarger
from ij.process import AutoThresholder
from ij.process import ImageStatistics as IS
from ij.process import StackConverter

from java.lang import System

from fiji.util.gui import GenericDialogPlus

# create generic dialog window to get user input
def getOptions():
	gd = GenericDialogPlus("Auto Crop Options")
	gd.addDirectoryField("select input folder", "E:\Users\Yoko Ito", 40)
	gd.addCheckbox("including subfolders", False)
	gd.addChoice("Specify reference channel", ["use RGB composite","1","2","3","4","5","6"], "1")
	gd.addNumericField("tissue margin to crop", 100, 2, 5, "micron")
	gd.addCheckbox("Calibrate image on the run", False)
	gd.addNumericField("pixel size:", 0.227, 3, 5, "micron")	
	gd.addCheckbox("Custom corner size", False)
	gd.addNumericField("corner size", 100, 2, 5, "micron")
	gd.addNumericField("corner threshold", 1.5, 2)
	gd.addStringField("image file extension", "tif", 4)
	gd.addCheckbox("store cropped images in a new folder (Y/N)", True)
	helpMessage = "This plugin automatically crop tissue margin based on pixel intensity of the image.\n \n"\
				+ "It will automatically detect margin of the tissue, and crop along the margin\n"\
				+ "with size defined by the user.\n"\
				+ "If there is no tissue margin detected in the image, the image will be left untouched\n \n"\
				+ "It takes a reference channel or the RGB composite, as user prefer, for tissue\n"\
				+ "margin evaluation. The tissue-void corner or border will be recognized if the\n"\
				+ "mean signal intensity is lower than a certain threshold when comparing to the\n"\
				+ "whole image.\n"\
				+ "User can define the size and threshold for corner detection, or left them as default.\n \n"\
				+ "The image is better to be calibrated to micron, or user can provide the pixel size on the run.\n"\
				+ "If left uncalibrated and no pixel size available, it will take 0.227 micron/pixel as default.\n \n"\
				+ "It is recommended to store the cropped image in a new folder, otherwise it will OVERWRITE\n"\
				+ "the original image, and might cost irreversible data loss!\n \n"\
				+ "It doesn't work well with small tissue sample currently.\n"\
				+ "For example band-shaped tissue image with margin on both sides will not be cropped correctly."
	gd.addMessage(helpMessage)
	gd.showDialog()
	if gd.wasCanceled():
		print "User canceled dialog!"
		return
	srcDir = gd.getNextString()
	includeSubDir = gd.getNextBoolean()
	refChannel = int(gd.getNextChoiceIndex())
	cropSize = gd.getNextNumber()
	cal = gd.getNextBoolean()
	pxSize = gd.getNextNumber()
	customCornerSize = gd.getNextBoolean()
	cornerSize = gd.getNextNumber()
	cornerThreshold = gd.getNextNumber()
	ext = gd.getNextString()
	createDstDir = gd.getNextBoolean()
	return srcDir, includeSubDir, refChannel, cropSize, cal, pxSize, customCornerSize, cornerSize, cornerThreshold, ext, createDstDir

# main function to run
def run():
	# get input parameter from generic dialog
	options = getOptions()  
	if options is None:
		return
	srcDir, includeSubDir, refChannel, cropSize, cal, pxSize, customCornerSize, cornerSize, cornerThreshold, ext, createDstDir = options
	# scan all the image files within the given folder
	scanFolder(srcDir, includeSubDir, ext, createDstDir, refChannel, cal, pxSize, customCornerSize, cornerSize, cornerThreshold, cropSize)	
	System.gc()
	return

# function to scan image files within a folder
def scanFolder(srcDir, includeSubDir, ext, createDstDir, refChannel, cal, pxSize, customCornerSize, cornerSize, threshold, cropSize):
	# get all the tif image files within the given folder
	fileList = []
	if includeSubDir:
		for root, directories, filenames in os.walk(srcDir):
			filenames.sort()
			for filename in filenames:
				if not filename.endswith(ext):
					continue
				if root.endswith("borderCropped"):
					continue
				fileList.append(os.path.join(root, filename))
	else:
		for filename in os.listdir(srcDir):
			if filename.endswith(ext): 
				fileList.append(os.path.join(srcDir, filename))
	fileList.sort()
	# check if the file list is empty
	if fileList:
		# process each tif image file within the file list
		for f in fileList:
			imp = IJ.openImage(f)
			#imp.show()
			# process the current image/imagestack
			c = 1
			if (imp.isStack()):				
				[width, height, nChannels, nSlices, nFrames] = imp.getDimensions()
				if (nChannels > 1):
					c = int(min(refChannel, nChannels))
			# evaluate if the current image contain corners of tissue
			#global customCornerSize, cornerSize
			sizeRoi, sizeCrop = getPhysicalSize(imp,cal,pxSize,customCornerSize,cornerSize,cropSize)
			cor1,cor2,cor3,cor4 = evaluateImage(imp,c,sizeRoi,threshold)
			# if corner found, crop the image base on reference channel
			if (cor1 | cor2 | cor3 | cor4):
				cornerStr = " "
				if cor1:
					cornerStr = cornerStr + " UP-LEFT"
				if cor2:
					cornerStr = cornerStr + " UP-RIGHT"
				if cor3:
					cornerStr = cornerStr + " BOTTOM-LEFT"
				if cor4:
					cornerStr = cornerStr + " BOTTOM-RIGHT"
				cornerStr = cornerStr + " Corner Found."
				print "Image: " + os.path.basename(f)
				print cornerStr

				border = evaluateBorder(imp,c,sizeRoi,threshold)
				if border:
					print "Tissue Border Found."
				
				if (c==0):
					ref_imp = imp.duplicate()
					ref_imp.setTitle("mask")
					stc = StackConverter(ref_imp)
					stc.convertToRGB()
					IJ.run(ref_imp,"8-bit","")
				else:
					ip = imp.getImageStack().getProcessor(c).duplicate()
					ref_imp = ImagePlus('mask',ip)

				all = cor1+cor2+cor3+cor4
				cor = int(0)

				if (all==2):
					UL_BR = (cor1 and cor4)
					UR_BL = (cor2 and cor3)
					if (UL_BR or UR_BL):
						cor = 4
					else:
						cor = 2
				elif (all==4):
					if border:
						cor = 6
					else:
						cor = 5
				else:
					cor = all						

				func = switcher.get(cor, "nothing")

				tissueRoi = func(ref_imp,cor1,cor2,cor3,cor4,sizeRoi,sizeCrop,refChannel)
					
				#ref_imp = ImagePlus("", imp.getImageStack().getProcessor(c))
				#tissueRoi = createSelection(ref_imp,cor1,cor2,cor3,cor4,sizeRoi,sizeCrop,refChannel)				
				if (tissueRoi == None):
					continue
				#newTissueRoi = roiEnlarger(tissueRoi)	
				croppedImage = cropImage(imp,c,tissueRoi)
				ref_imp.changes = False
				ref_imp.close()	
				# if specified, store the cropped image in a new folder
				if createDstDir:
					parentDir = os.path.dirname(f)
					cropDir = os.path.join(parentDir, "borderCropped")
					if not os.path.exists(cropDir):
						os.makedirs(cropDir)
					dstFile = os.path.join(cropDir,os.path.basename(f))
				# otherwise cropped image will overwrite the original image!	
				else:
					dstFile = f
				fs = FileSaver(croppedImage)
				fs.saveAsTiff(dstFile)
			# operation complete
			imp.close()
			IJ.run("Collect Garbage", "")
		#Prefs.closingAll()	
		System.gc()
	return
	
# Set of functions to crop corner accordingly
def noCorner(imp,UL,UR,BL,BR,sizeRoi,sizeCrop,refChannel):
	print "no corner"
	return None
def oneCorner(imp,UL,UR,BL,BR,sizeRoi,sizeCrop,refChannel):
	print "1 corner"
	#IJ.run(imp, "Make Inverse", "");
	roi = createSelection(imp,~UL,~UR,~BL,~BR,sizeRoi,-sizeCrop,refChannel)
	return roi
def twoCorner1(imp,UL,UR,BL,BR,sizeRoi,sizeCrop,refChannel):
	print "2 corners on the same side"
	roi = createSelection(imp,UL,UR,BL,BR,sizeRoi,sizeCrop,refChannel)
	return roi
def twoCorner2(imp,UL,UR,BL,BR,sizeRoi,sizeCrop,refChannel):
	print "2 corners on the opposite side"
	return	None
def threeCorner(imp,UL,UR,BL,BR,sizeRoi,sizeCrop,refChannel):
	print "3 corners"
	roi = createSelection(imp,UL,UR,BL,BR,sizeRoi,sizeCrop,refChannel)
	return roi
def fourCorner1(imp,UL,UR,BL,BR,sizeRoi,sizeCrop,refChannel):
	print "4 corners with tissue in the middle"
	roi = createSelection(imp,UL,UR,BL,BR,sizeRoi,sizeCrop,refChannel)
	return roi
def fourCorner2(imp,UL,UR,BL,BR,sizeRoi,sizeCrop,refChannel):
	print "4 corners with tissue passing by"
	return None
	
switcher = {
	0: noCorner,
	1: oneCorner,
	2: twoCorner1,
	4: twoCorner2,
	3: threeCorner,
	5: fourCorner1,
	6: fourCorner2
	}
# function to get the corner size
def getPhysicalSize(imp,cal,pxSize,customCornerSize,cornerSize,cropSize):
	# get corner size in micron, default is 100 micron
	if customCornerSize:
		size = cornerSize
	else:
		size = 100
	# if image not calibrated, use 300 pixel as corner size
	if cal:
		sizePx = size/pxSize
		sizeCrop = cropSize/pxSize 
	elif (imp.getCalibration().getUnit() == "inch"):
		IJ.redirectErrorMessages()
		IJ.error("Calibration error","Image appear to be not calibrated, use 0.227 micron/pixel scale.")
		sizePx = size/0.227
		sizeCrop = cropSize/0.227
	else:
		sizePx = (imp.getCalibration().getRawX(size))
		sizeCrop = (imp.getCalibration().getRawX(cropSize))
	return int(sizePx), int(sizeCrop)

# function to evaluate if images containing tissue corner	
def evaluateImage(imp, channel, sizeRoi, threshold):
	# if channel is 0, use RGB composite to evaluate
	if (channel==0):
		eval_imp = imp.duplicate()
		eval_imp.setTitle("eval")
		stc = StackConverter(eval_imp)
		stc.convertToRGB()
		IJ.run(eval_imp,"8-bit","")
	# if input is image stack, use reference channel to evaluate
	elif (imp.isStack()):
		eval_ip = imp.getImageStack().getProcessor(channel)
		eval_imp = ImagePlus("eval",eval_ip)
	else:
		eval_imp = imp.duplicate()
		eval_imp.setTitle("eval")
		IJ.run(eval_imp,"8-bit","")	
	# set X-Y coordinates for triangle ROIs	
	xSet_UL = [0, sizeRoi, 0]
	ySet_UL = [0, 0, sizeRoi]
	xSet_UR = [eval_imp.getWidth()-sizeRoi, eval_imp.getWidth(), eval_imp.getWidth()]
	ySet_UR = [0, 0, sizeRoi]
	xSet_BL = [0, sizeRoi, 0]
	ySet_BL = [eval_imp.getHeight()-sizeRoi, eval_imp.getHeight(), eval_imp.getHeight()]
	xSet_BR = [eval_imp.getWidth()-sizeRoi, eval_imp.getWidth(), eval_imp.getWidth()]
	ySet_BR = [eval_imp.getHeight(), eval_imp.getHeight(), eval_imp.getHeight()-sizeRoi]
	# evaluate four corners of the image	
	UL_TF = evaluateCorner(eval_imp,xSet_UL,ySet_UL,threshold)
	UR_TF = evaluateCorner(eval_imp,xSet_UR,ySet_UR,threshold)
	BL_TF = evaluateCorner(eval_imp,xSet_BL,ySet_BL,threshold)
	BR_TF = evaluateCorner(eval_imp,xSet_BR,ySet_BR,threshold)
	eval_imp.changes = False
	eval_imp.close()
	return UL_TF, UR_TF, BL_TF, BR_TF

# in case of all 4 corners found, check whether the tissue is a band passing through the image
def evaluateBorder(imp, channel, sizeRoi, threshold):
	# if channel is 0, use RGB composite to evaluate
	if (channel==0):
		eval_imp = imp.duplicate()
		eval_imp.setTitle("eval")
		stc = StackConverter(eval_imp)
		stc.convertToRGB()
		IJ.run(eval_imp,"8-bit","")
	# if input is image stack, use reference channel to evaluate
	elif (imp.isStack()):
		eval_ip = imp.getImageStack().getProcessor(channel)
		eval_imp = ImagePlus("eval",eval_ip)
	else:
		eval_imp = imp.duplicate()
		eval_imp.setTitle("eval")
		IJ.run(eval_imp,"8-bit","")	
	# get the mean pixel value of the entire image
	w = eval_imp.getWidth()
	h = eval_imp.getHeight()
	r = sizeRoi
	eval_imp.setRoi(0,0,w,h)
	img_mean = getStatistics(eval_imp)
	# set rectangle ROI to the border
	U_roi = Roi(round((w-r)/2),0,r,r)
	B_roi = Roi(round((w-r)/2),h-r,r,r)
	L_roi = Roi(0,round((h-r)/2),r,r)
	R_roi = Roi(w-r,round((h-r)/2),r,r)
	# get the mean pixel value within the triangle ROI
	eval_imp.setRoi(U_roi)
	U_mean = getStatistics(eval_imp)
	eval_imp.setRoi(B_roi)
	B_mean = getStatistics(eval_imp)
	eval_imp.setRoi(L_roi)
	L_mean = getStatistics(eval_imp)
	eval_imp.setRoi(R_roi)
	R_mean = getStatistics(eval_imp)
	# reset ROI
	eval_imp.deleteRoi()
	# if mean pixel value is 0, the corner is a tissue margin
	if ((U_mean+B_mean+L_mean+R_mean) == 0):
		return True
	# if the ratio of the mean pixel value of the curret corner to the
	# entire image is lower than threshold (significantly darker), the 
	# corner is a tissue margin
	if (img_mean/U_mean > threshold):
		return True	
	elif (img_mean/B_mean > threshold):
		return True
	elif (img_mean/L_mean > threshold):
		return True	
	elif (img_mean/R_mean > threshold):
		return True
	else:
		return False	
		
# function to crop image/image stack
def getStatistics(imp):  
  """ Return statistics (mean) for the given ImagePlus """  
  ip = imp.getProcessor()  
  stats = IS.getStatistics(ip, IS.MEAN, imp.getCalibration())  
  return stats.mean

# function to detect corner of tissue  
def evaluateCorner(imp,xSet,ySet,threshold):
	# get the mean pixel value of the entire image
	imp.setRoi(0,0,imp.getWidth(),imp.getHeight())
	img_mean = getStatistics(imp)
	# set triangle ROI to the corner
	proi = PolygonRoi(xSet,ySet,len(xSet),Roi.POLYGON)
	imp.setRoi(proi);
	# get the mean pixel value within the triangle ROI
	corner_mean = getStatistics(imp)
	# reset ROI
	imp.deleteRoi()
	# if mean pixel value is 0, the corner is a tissue margin
	if (corner_mean == 0):
		return True
	# if the ratio of the mean pixel value of the curret corner to the
	# entire image is lower than threshold (significantly darker), the 
	# corner is a tissue margin
	if (img_mean/corner_mean > threshold):
		return True	
	else:
		return False
		
# function to fill the corner with foreground color
def fillCorner(imp,X,Y,sizeX,sizeY):
	ip = imp.getProcessor()
	ip.setValue(ip.getMax())
	ip.fillRect(X,Y,sizeX,sizeY)
	imp.deleteRoi()
	
# function to create polygon roi selection from input image
def createSelection(imp,UL,UR,BL,BR,sizeRoi,sizeCrop,refChannel):
	ip = imp.getProcessor()
	width = imp.getWidth()
	height = imp.getHeight()
	# RGB image need special processing
	if (refChannel==0):
		ip.setAutoThreshold("Otsu", True, 0)
		IJ.run(imp, "Convert to Mask", "")
		IJ.run(imp, "Median...", "radius=5")
	else:
		ip.setAutoThreshold("Triangle", True, 0)
		IJ.run(imp, "Convert to Mask", "")
		IJ.run(imp, "Median...", "radius=5")

	if not (UL):
		fillCorner(imp, 0, 0, sizeRoi, sizeRoi)
	if not (UR):
		fillCorner(imp, imp.getWidth()-sizeRoi, 0, sizeRoi, sizeRoi)
	if not (BL):
		fillCorner(imp, 0, imp.getHeight()-sizeRoi, sizeRoi, sizeRoi)
	if not (BR):
		fillCorner(imp, imp.getWidth()-sizeRoi, imp.getHeight()-sizeRoi, sizeRoi, sizeRoi)

	# generate convex hull selection
  	IJ.run(imp, "Convex Hull Plus", "mode=[Convex Hull selection] white log")
	# fill the convex hull with foreground color
  	ip = imp.getProcessor()
	pg = imp.getRoi().getPolygon()
  	#tp = imp.getRoi().getType()
	x = pg.xpoints
	y = pg.ypoints
	
	x_UL = 0
	y_UL = 0
	x_UR = 0
	y_UR = 0
	x_BL = 0
	y_BL = 0
	x_BR = 0
	y_BR = 0
	if UL:
		x_UL = 1
		y_UL = 1	
	if UR:
		x_UR = -1
		y_UR = 1
	if BL:
		x_BL = 1
		y_BL = -1	
	if BR:
		x_BR = -1
		y_BR = -1
	xAll = (x_UL + x_UR + x_BL + x_BR)
	yAll = (y_UL + y_UR + y_BL + y_BR)
	if (xAll==0 and yAll==0):
		# no corner then return none selection
		if not (UL or UR or BL or BR):
			return None
		# all 4 corners with tissue in the middle	
		else:
			a, b = divmod(sizeCrop, 255)
			for i in range(0, int(a+1)):
				if (i==a):
					IJ.run(imp, "Enlarge...", "enlarge=[-"+str(int(b))+"]");
				else:
					IJ.run(imp, "Enlarge...", "enlarge=-255");
			return imp.getRoi()
	if (xAll > 0):
		new_x = [i+sizeCrop for i in x]
	elif (xAll <0):
		new_x = [i-sizeCrop for i in x]
	else:
		new_x = x	
	if (yAll > 0):
		new_y = [i+sizeCrop for i in y]
	elif (yAll < 0):
		new_y = [i-sizeCrop for i in y]	
	else:
		new_y = y
	new_x = map(int, new_x)
	new_y = map(int, new_y)	
	new_pg = PolygonRoi(new_x, new_y, len(new_y), 2)
	#imp.setRoi(new_pg)
	#return imp.getRoi()
	return new_pg
	

def cropImage(oriImp,nChannels,tissueRoi):
	stack = oriImp.getImageStack()
	for i in xrange(1,stack.getSize()+1):
		IJ.showProgress(i,stack.getSize()+1)
		ip = stack.getProcessor(i)
		ip.setValue(ip.getBackgroundValue())
		ip.fillOutside(tissueRoi)
	return oriImp
		
run()
IJ.showProgress(1) 